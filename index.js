/*
  Ejercicio: Instalar Node.js, crear y ejecutar un programa en Node.js
  Autor: Veselin Georgiev | veselingp@hotmail.com
  Descripción: Hola Node!
*/
// Inicializamos módulos a utilizar
var http = require('http');
// Creamos servidor HTTP
http.createServer(function(peticion, respuesta){
	console.log('Archivo solicitado');
	respuesta.writeHead(200, {'Content-Type': 'text/plain'});
	respuesta.end('Hola Node!');
}).listen(8080);
console.log('Servidor ejecutándose en: http://127.0.0.1:8080/');